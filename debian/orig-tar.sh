#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>
TAR=../libcobra-java_$2.orig.tar.gz
DIR=libcobra-java-$2.orig

# clean up the upstream tarball
unzip $3
mv cobra-$2 $DIR
GZIP=--best tar -c -z -f $TAR --exclude '*.jar' --exclude '*/doc/api/*' $DIR
rm -rf $3 $DIR

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir
  echo "moved $TAR to $origDir"
fi

exit 0
